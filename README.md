Panduan Sainsterbuka
---

# Tujuan:

Panduan ini dibuat untuk para peneliti dan dosen (P/D) yang berminat
untuk mengimplementasikan konsep sainsterbuka dalam riset atau dalam
alur kerjanya.

# Tim (sesuai abjad):

- Afrilya [IGDORE](https://igdore.org/)
- Dasapta Erwin Irawan, Institut Teknologi Bandung ([ORCID](http://orcid.org/0000-0002-1526-0863))
- Juneman Abraham, Universitas Bina Nusantara ([ORCID](http://orcid.org/0000-0003-0232-2735))
- Sami Kandha
- Surya Darma [IGDORE](https://igdore.org/)
- aaa
- bbb
- ccc
- ddd
- eee
- ffff

** Want to join our band wagon? **


# Misi:

Mempromosikan konsep sainsterbuka ke dalam alur kerja riset yang
memiliki beberapa nilai sebagai berikut:

- terbuka
- transparan
- kolaborasi yang inklusif

# Draft bab

Pada draft pertama ini, kami membuat sebanyak tujuh bab:

- Bab 1 Pendahuluan
- Bab 2 Sekilas kondisi riset dan publikasi saat ini
- Bab 3 Konsep sainsterbuka
- Bab 4 Implementasi sainsterbuka
- Bab 5 Contoh kasus
- Bab 6 Penutup
- Bab 7 Referensi


